import sys
import shutil
import h5py
import numpy

time_mapping = []
time_mapping.append(('Time:  0.00000E+00 y','plot.0000'))
time_mapping.append(('Time:  5.47945E-03 y','plot.0048'))
time_mapping.append(('Time:  2.73973E-02 y','plot.0056'))
time_mapping.append(('Time:  1.36986E-01 y','plot.0065'))
time_mapping.append(('Time:  2.73973E-01 y','plot.0069'))
time_mapping.append(('Time:  1.00000E+00 y','plot.0077'))
time_mapping.append(('Time:  2.00000E+00 y','plot.0081'))
time_mapping.append(('Time:  1.00000E+01 y','plot.0089'))
time_mapping.append(('Time:  1.00000E+02 y','plot.0100'))
time_mapping.append(('Time:  1.00000E+03 y','plot.0113'))

filename = './combined_output.h5'

# copy the PFLOTRAN output file
shutil.copy('pflotran/tensorial_rel_perm.h5',filename)

h5file = h5py.File(filename,mode='r+')
# append a dataset named STOMP_Sat at each time with STOMP results
for time in time_mapping:
    print(time)
    group = h5file['/'+time[0]]
    dataset = h5file['/'+time[0]+'/Liquid_Saturation']
    n = 1
    for axis in dataset.shape:
        n *= axis
    array = numpy.zeros(n,dtype=float)
    filename = './stomp/' + time[1]
    f = open(filename,'r')
    count = -1
    for line in f:
        if (line.startswith('Aqueous Saturation')):
            count = 0
            continue
        if count >= 0:
            words = line.strip().split()
            for word in words:
                array[count] = float(word)
                count += 1
            if count >= n:
                break
    f.close()
    array3d = numpy.zeros(dataset.shape,dtype=float)
    count = 0
    for k in range(dataset.shape[2]):
        for j in range(dataset.shape[1]):
            for i in range(dataset.shape[0]):
                array3d[i][j][k] = array[count]
                count += 1
    group.create_dataset('STOMP_Sat',data=array3d)
    i = 5
    j = 5
    print('    PFLOTRAN             STOMP')
    for k in range(dataset.shape[2]):
        print(k,dataset[i,j,k],array3d[i,j,k])

h5file.close()
print('done with everything')
  
